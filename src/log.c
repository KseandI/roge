#include <roge.h>

WINDOW* win_log;

any
init_log()
{
  any _win_log = get_window_log();

  if (_win_log == NULL)
    return (any)NOERR;

  win_log = (WINDOW*) _win_log;

  return (any)NOERR;
}

any
printlog(const char* _text)
{
  mvwaddstr(
      win_log,
      1,
      1,
      _text
      );

  return (any)NOERR;
}
