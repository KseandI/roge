#include <roge.h>

any
init_all()
{

  init_display();
  init_curs();
  init_object();
  init_log();
  return (any)NOERR;
}

any
deinit_all()
{

  deinit_curs();
  deinit_display();
  deinit_object();
  return (any)NOERR;
}
