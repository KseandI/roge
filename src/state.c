#include <roge.h>

int game_state = 0;

int
game_is_running()
{
  return game_state;
}

any
game_stop()
{
  game_state = 0;
  return (any)NOERR;
}

any
game_start()
{
  game_state = 1;
  return (any)NOERR;
}

any wall[1025];
any player;

any
startup()
{
  for (uint_t i = 1; i < 1024; i++)
  {
    wall[i] = create_wall(i/32, i%32);
    add_object(wall[i]);
  }
  player = create_player(2, 3);
  add_object(player);
  set_centered_object(player);
  printlog("Welcome to roge");
  display_redraw();
  return (any)NOERR;
}

any
loop()
{
  
  int gch = 0;
  while (game_is_running()) 
  {
    display_all();
    draw_display();
    display_redraw();

    do {gch = getch();} while(gch == ERR);
    
    switch (gch)
    {

      case (char)'h':
	player_move(player, -1, 0);
	break;

      case (char)'l':
	player_move(player, 1, 0);
	break;

      case (char)'j':
	player_move(player, 0, 1);
	break;

      case (char)'k':
	player_move(player, 0, -1);
	break;

      case (char)'q':
	game_stop();

      default:
	break;
    }
    display_clear();
    char* log_text = (char*) malloc(sizeof(char)*127);
    sprintf(log_text, "x: %d, y: %d", ((struct object*)player)->x, ((struct object*)player)->y);
    printlog(log_text);
  }
  return (any)NOERR;
}
