#ifndef _ROGE_H_
#define _ROGE_H_

#include <ncurses.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef void* any;
typedef unsigned int uint_t;

struct object
{
  int x;
  int y;
  chtype ch;
};

struct object_array
{
  struct object** objects;
  uint_t len;
};

enum ROGE_ERRORS
{
  NOERR,
  ERROR_NULL,
  ERROR_MEMORY,
};

// roge.c
any roge_startup();
any roge_end();

// init.c
any init_all();
any deinit_all();

// curs.c
any get_window_game();
any get_window_log();
int get_game_height();
int get_game_width();
any init_curs();
any deinit_curs();

// display.c
any init_display();
any draw_display();
any display_redraw();
any deinit_display();
any display_all();
any display_list(any _objects);
any display_object(any _object);
any display_object_offset(any _object, int _x, int _y);
any set_centered_object(any _object);
any display_clear();

// object.c
any init_object();
any deinit_object();
any add_object(any _object);
any get_objects();

// state.c
any loop();
int game_is_running();
any game_start();
any game_stop();
any startup();

// wall.c
any create_wall(int _x, int _y);

// player.c
any create_player(int _x, int _y);
any player_move(any _player, int _x, int _y);

// log.c
any init_log();
any printlog(const char* _text);

#endif // _ROGE_H_
